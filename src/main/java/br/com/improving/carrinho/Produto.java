package br.com.improving.carrinho;

/**
 * Classe que representa um produto que pode ser adicionado
 * como item ao carrinho de compras.
 *
 * Importante: Dois produtos são considerados iguais quando ambos possuem o
 * mesmo código.
 */
public class Produto {

    private Long codigo;
    private String descricao;

    /**
     * Construtor da classe Produto.
     *
     * @param codigo
     * @param descricao
     */
    public Produto(Long codigo, String descricao) {
    }

    /**
     * Retorna o código da produto.
     *
     * @return Long
     */
    public Long getCodigo() {
    	return codigo;
    }

    /**
     * Retorna a descrição do produto.
     *
     * @return String
     */
    public String getDescricao() {
    	return descricao;
    }

    @Override
	public String toString(){
    	return "Produto [descricao=" + descricao + "]";
	}

	@Override
	public int hashCode(){
    	final int prime = 31;
    	int resultado = 1;
    	resultado = prime * resultado + ((codigo == null) ? 0 : codigo.hashCode());
    	return resultado;
	}
}